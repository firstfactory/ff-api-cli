import * as shell from 'shelljs';
import * as fs from 'fs-extra';

let appDirectory = '';

function createServerless(): Promise<boolean> {
  return new Promise((resolve) => {
    console.log('\nCreating serverless api...'.cyan);
    try {
      shell.exec(
        `serverless create --template aws-nodejs-typescript`,
        (e, _stdout, stderr) => {
          if (e !== 0) {
            console.log('E: ', e);
            console.log('Stderr: ', stderr);
            resolve(false);
          } else {
            console.log('Finished creating serverless api'.green);
            resolve(true);
          }
        },
      );
    } catch (e) {
      console.log('serverless@2.64.1 not installed'.red);
      resolve(false);
      process.exit(1);
    }
  });
}

function installPackages(): Promise<void> {
  return new Promise((resolve) => {
    console.log('\nInstalling packages...'.cyan);
    const command =
      'npm install' +
      ' @nestjs/common' +
      ' @nestjs/core' +
      ' @nestjs/platform-express' +
      ' aws-serverless-express' +
      ' reflect-metadata' +
      ' rxjs' +
      ' @prisma/client@4.6.1' +
      ' @nestjs/apollo' +
      ' @nestjs/graphql' +
      ' graphql-tools@8.3.12' +
      ' graphql@16.5.0' +
      ' apollo-server-express' + // deprecated, use @apollo/server
      ' @nestjs/config' +
      ' class-validator' +
      ' class-transformer' +
      ' ts-morph@16.0.0' +
      ' @apollo/gateway';
    shell.exec(command, { cwd: appDirectory }, () => {
      console.log('\nFinished installing packages\n'.green);
      resolve();
    });
  });
}

function installDevPackages(): Promise<void> {
  return new Promise((resolve) => {
    console.log('\nInstalling dev packages...'.cyan);
    const command =
      'npm install --save-dev' +
      ' @types/node' +
      ' @types/express' +
      ' @types/aws-serverless-express' +
      ' serverless-offline' +
      ' @nestjs/cli' +
      ' @nestjs/schematics' +
      ' prisma@4.6.1' +
      ' copy-webpack-plugin' +
      ' webpack' +
      ' serverless-webpack@5.6.1 serverless-webpack-prisma@1.0.9' +
      ' webpack-node-externals@2.5.2' +
      ' ts-loader';
    shell.exec(command, { cwd: appDirectory }, () => {
      console.log('\nFinished installing dev packages\n'.green);
      resolve();
    });
  });
}

function initPrisma(): Promise<void> {
  return new Promise((resolve) => {
    console.log('\nInit prisma...'.cyan);
    const command =
      'npx prisma init';
    shell.exec(command, { cwd: appDirectory }, () => {
      console.log('\nPrisma has been initialized\n'.green);
      resolve();
    });
  });
}

function generateBoilerplate(): void {
  console.log('\nGenerating boilerplate...'.cyan);
  if (require.main) {
    const basePath = require('path').dirname(require.main.filename) + `/content/`;
    const srcSchemaFile = `${basePath}/prisma/schema.txt`;
    const schemaFile = `${appDirectory}/prisma/schema.prisma`;
    fs.copySync(srcSchemaFile, schemaFile);

    const srcCourseType = `${basePath}/src/student/types/course.txt`;
    const courseType = `${appDirectory}/src/student/types/course.ts`;
    fs.copySync(srcCourseType, courseType);

    const srcStudentType = `${basePath}/src/student/types/student.txt`;
    const studentType = `${appDirectory}/src/student/types/student.ts`;
    fs.copySync(srcStudentType, studentType);

    const srcStudentModule = `${basePath}/src/student/student.module.txt`;
    const studentModule = `${appDirectory}/src/student/student.module.ts`;
    fs.copySync(srcStudentModule, studentModule);

    const srcStudentResolver = `${basePath}/src/student/student.resolver.txt`;
    const studentResolver = `${appDirectory}/src/student/student.resolver.ts`;
    fs.copySync(srcStudentResolver, studentResolver);

    const srcAppModule = `${basePath}/src/app.module.txt`;
    const appModule = `${appDirectory}/src/app.module.ts`;
    fs.copySync(srcAppModule, appModule);

    const srcContextInterface = `${basePath}/src/context.interface.txt`;
    const contextInterface = `${appDirectory}/src/context.interface.ts`;
    fs.copySync(srcContextInterface, contextInterface);

    const srcEnvFile = `${basePath}/env.txt`;
    const envFile = `${appDirectory}/.env`;
    fs.copySync(srcEnvFile, envFile);

    const srcGitIgnore = `${basePath}/gitignore.txt`;
    const gitIgnore = `${appDirectory}/.gitignore`;
    fs.copySync(srcGitIgnore, gitIgnore);

    const srcHandler = `${basePath}/handler.txt`;
    const handler = `${appDirectory}/handler.ts`;
    fs.copySync(srcHandler, handler);

    const srcServerless = `${basePath}/serverless.txt`;
    const serverless = `${appDirectory}/serverless.ts`;
    fs.copySync(srcServerless, serverless);

    const srctsconfig = `${basePath}/tsconfig.txt`;
    const tsConfig = `${appDirectory}/tsconfig.json`;
    fs.copySync(srctsconfig, tsConfig);

    const srctsBuildconfig = `${basePath}/tsconfig.build.txt`;
    const tsBuildConfig = `${appDirectory}/tsconfig.build.json`;
    fs.copySync(srctsBuildconfig, tsBuildConfig);

    const srctsPathsconfig = `${basePath}/tsconfig.paths.txt`;
    const tsPathsConfig = `${appDirectory}/tsconfig.paths.json`;
    fs.copySync(srctsPathsconfig, tsPathsConfig);

    const srcMain = `${basePath}/src/main.txt`;
    const main = `${appDirectory}/src/main.ts`;
    fs.copySync(srcMain, main);

    const srcLibApiGateway = `${basePath}/src/libs/apiGateway.txt`;
    const libApiGateway = `${appDirectory}/src/libs/apiGateway.ts`;
    fs.copySync(srcLibApiGateway, libApiGateway);

    const srcHandlerResolver = `${basePath}/src/libs/handlerResolver.txt`;
    const handlerResolver = `${appDirectory}/src/libs/handlerResolver.ts`;
    fs.copySync(srcHandlerResolver, handlerResolver);

    const srcLambda = `${basePath}/src/libs/lambda.txt`;
    const lambda = `${appDirectory}/src/libs/lambda.ts`;
    fs.copySync(srcLambda, lambda);

    const srcFunctionHelloHandler = `${basePath}/src/functions/hello/handler.txt`;
    const functionHelloHandler = `${basePath}/src/functions/hello/handler.ts`;
    fs.copySync(srcFunctionHelloHandler, functionHelloHandler);

    const srcFunctionHelloIndex = `${basePath}/src/functions/hello/index.txt`;
    const functionHelloIndex = `${basePath}/src/functions/hello/index.ts`;
    fs.copySync(srcFunctionHelloIndex, functionHelloIndex);

    const srcFunctionHelloMock = `${basePath}/src/functions/hello/mock.txt`;
    const functionHelloMock = `${basePath}/src/functions/hello/mock.json`;
    fs.copySync(srcFunctionHelloMock, functionHelloMock);

    const srcFunctionHelloSchema = `${basePath}/src/functions/hello/schema.txt`;
    const functionHelloSchema = `${basePath}/src/functions/hello/schema.ts`;
    fs.copySync(srcFunctionHelloSchema, functionHelloSchema);

    const srcFunctionIndex = `${basePath}/src/functions/index.txt`;
    const functionIndex = `${basePath}/src/functions/index.ts`;
    fs.copySync(srcFunctionIndex, functionIndex);
  }
}

function executePrismaIntrospection(): Promise<void> {
  return new Promise((resolve) => {
    console.log('\nIntrospecting database...'.cyan);
    const command =
      'npx prisma db pull';
    shell.exec(command, { cwd: appDirectory }, () => {
      console.log('\nSchema has been updated\n'.green);
      resolve();
    });
  });
}

export const createServerlessApi = async () => {
  appDirectory = `${process.cwd()}`;
  const serverless = shell.which('serverless');
  if (!serverless) {
    console.log(
      `serverless not installed \n install serverless first globally using :`
        .red,
    );
    console.log(`npm install -g serverless`.white);
    process.exit(1);
  }
  const success = await createServerless();
  if (!success) {
    console.log(
      'Something went wrong while trying to create a new Serverless app using serverless'
        .red,
    );
    process.exit(1);
  } else {
    await installPackages();
    await installDevPackages();
    await initPrisma();
    generateBoilerplate();
    await executePrismaIntrospection();
    shell.exec(`yarn install`, { cwd: appDirectory }, () => {
      console.log('All done'.green);
      console.log('To start running the project, please add the following two scripts to package.json'.yellow)
      console.log(`"dev": "nest start --watch", "start": "npx prisma generate && ts-node main.ts", "offline": "sls offline --httpPort 4000"`.cyan)
    });
  }
}
