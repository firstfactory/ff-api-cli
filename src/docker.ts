import * as shell from 'shelljs';
import * as fs from 'fs-extra';

let appDirectory = '';
let appName = '';

function createNest(): Promise<boolean> {
  return new Promise((resolve) => {
    console.log('\nCreating Nest api...'.cyan);
    try {
      shell.exec(
        `nest new ${appName} --skip-install`,
        (e, _stdout, stderr) => {
          if (e !== 0) {
            console.log('E: ', e);
            console.log('Stderr: ', stderr);
            resolve(false);
          } else {
            console.log('Finished creating Nest api'.green);
            resolve(true);
          }
        },
      );
    } catch (e) {
      console.log('Nest not installed'.red);
      resolve(false);
      process.exit(1);
    }
  });
}

function installPackages(): Promise<void> {
  return new Promise((resolve) => {
    console.log('\nInstalling packages...'.cyan);
    const command =
      `yarn install && yarn add` +
      ' @prisma/client' +
      ' apollo-server-express' +
      ' type-graphql' +
      ' typegraphql-nestjs' +
      ' @nestjs/graphql' +
      ' graphql' +
      ' graphql-type-json' +
      ' graphql-fields' +
      ' class-validator';
    shell.exec(command, { cwd: appDirectory }, () => {
      console.log('\nFinished installing packages\n'.green);
      resolve();
    });
  });
}

function installDevPackages(): Promise<void> {
  return new Promise((resolve) => {
    console.log('\nInstalling dev packages...'.cyan);
    const command =
      `yarn add -D` +
      ' @prisma/cli' +
      ' typegraphql-prisma';
    shell.exec(command, { cwd: appDirectory }, () => {
      console.log('\nFinished installing dev packages\n'.green);
      resolve();
    });
  });
}

function initPrisma(): Promise<void> {
  return new Promise((resolve) => {
    console.log('\nInit prisma...'.cyan);
    const command =
      `npx prisma init`;
    shell.exec(command, { cwd: appDirectory }, () => {
      console.log('\nPrisma has been initialized\n'.green);
      resolve();
    });
  });
}

function generateBoilerplate(): void {
  console.log('\nGenerating boilerplate...'.cyan);
  fs.unlinkSync(`${appDirectory}/src/app.service.ts`);
  fs.unlinkSync(`${appDirectory}/src/app.controller.ts`);
  fs.unlinkSync(`${appDirectory}/src/app.controller.spec.ts`);
  if (require.main) {
    const basePath = require('path').dirname(require.main.filename) + `/content-docker/`;
    const srcSchemaFile = `${basePath}/prisma/schema.txt`;
    const schemaFile = `${appDirectory}/prisma/schema.prisma`;
    fs.copySync(srcSchemaFile, schemaFile);

    const srcAppModule = `${basePath}/src/app.module.txt`;
    const appModule = `${appDirectory}/src/app.module.ts`;
    fs.copySync(srcAppModule, appModule);

    const srcMain = `${basePath}/src/main.txt`;
    const main = `${appDirectory}/src/main.ts`;
    fs.copySync(srcMain, main);

    const srcEnvFile = `${basePath}/env.txt`;
    const envFile = `${appDirectory}/.env`;
    fs.copySync(srcEnvFile, envFile);

    const srcGitIgnore = `${basePath}/gitignore.txt`;
    const gitIgnore = `${appDirectory}/.gitignore`;
    fs.copySync(srcGitIgnore, gitIgnore);
  }
}

function executePrismaIntrospection(): Promise<void> {
  return new Promise((resolve) => {
    console.log('\nIntrospecting database...'.cyan);
    const command =
      `npx prisma introspect && npx prisma generate`;
    shell.exec(command, { cwd: appDirectory }, () => {
      console.log('\nSchema has been updated\n'.green);
      resolve();
    });
  });
}

export const createDockerApi = async (app: string) => {
  appName = app;
  appDirectory = `${process.cwd()}/${appName}`;
  const serverless = shell.which('nest');
  if (!serverless) {
    console.log(
      `Nest is not installed \n install nest first globally using :`
        .red,
    );
    console.log(`npm install -g @nestjs/cli`.white);
    process.exit(1);
  }
  const success = await createNest();
  if (!success) {
    console.log(
      'Something went wrong while trying to create a new nest app using nest/cli'
        .red,
    );
    process.exit(1);
  } else {
    await installPackages();
    await installDevPackages();
    await initPrisma();
    generateBoilerplate();
    await executePrismaIntrospection();
    console.log('All done'.green);
  }
}