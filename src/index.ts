#!/usr/bin/env node
import * as program from 'commander';
import 'colors';
import { createServerlessApi } from './serverless';
import { createDockerApi } from './docker';

program
  .name('ff-api')
  .version('1.0.0');
program.command('init-serverless').action(createServerlessApi);
program.command('init-docker <app>').action(createDockerApi);
program.parse(process.argv);