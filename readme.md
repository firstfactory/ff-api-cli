# Main Packages used
1. serverless framework
1. nestjs
1. prisma
1. graphql
1. apollo

# Important notes
Use node version 18.11 or higher for this project to work on from now on

# To install as command line locally. Within the ff-api-cli folder
npm run compile
npm install -g ./

# To Run within the folder of the new project and create a serverless API
ff-api init-serverless

# To Run providing a dir/project name and create a docker API
ff-api init-docker directory